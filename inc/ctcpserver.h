#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QTcpServer>
#include <QThread>
#include <QTcpSocket>
#include <QDebug>
#include <QTime>
#include <QMessageBox>

#define TCP_TIMEOUT 1000

class ctcpserver : public QTcpServer
{
  Q_OBJECT

  public:
    explicit    ctcpserver(QTcpServer *parent = nullptr);
    bool        m_autoresp;
    bool        m_bRandom;
    QString     m_strResp;
    quint16     m_nPort;
    QTcpSocket* m_clientSock;

  signals:
    void        signStrToLog(QString istr);

  public slots:
    void        slotNewConnection     ();
    void        slotReadClient        ();
    void        slotSendToClient      (QByteArray dat);
    void        bytesWritten          (qint64 bytes);
    void        slotClientDisconnected();
    void        StartServer           ();
    void        StopServer            ();
};

#endif // TCPSERVER_H
