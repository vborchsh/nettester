#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>

#include <./inc/functions.h>
#include <./inc/ctcpclient.h>
#include <./inc/ctcpserver.h>
#include <./inc/ccomport.h>

#define VER 1
#define SUB 6
#define REV 2

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private:
  Ui::MainWindow*   ui;
  QThread           m_ServerThread;
  QThread           m_ClientThread;

  ctcpclient*       m_Client;
  ctcpserver*       m_Server;
  CComPort*         m_Comport;

  QByteArray        m_arr_servtoclient;
  bool              m_bServStatus;
  bool              m_bClientStatus;

  quint16           m_port;
  QString           m_addr;

  // Main menu
  QMenu*            menuFile;
  QAction*          actionExit;
  QAction*          actionOpenAbout;

  // Tray menu
  QMenu*            trayIconMenu;
  QAction*          minimizeAction;
  QAction*          restoreAction;
  QAction*          quitAction;
  QAction*          startServerAction;
  QAction*          stopServerAction;
  QSystemTrayIcon*  trayIcon;

signals:
  void              signSendToClient(QByteArray dat);
  void              signReadClient  ();
  void              signSentToServ  (QByteArray dat);
  void              signReadServ    ();

public slots:
  void              slotToServerLog(QString istr);
  void              slotToClientLog(QString istr);
  void              slotToComLog(QString istr);

private slots:
  void              on_m_btn_StartServ_clicked();
  void              on_m_btn_StopServ_clicked();
  void              SetGuiServ(bool st);
  void              SetGuiClient(bool st);
  void              on_m_btn_SendServer_clicked();
  void              on_m_btn_StartClient_clicked();
  void              on_m_btn_StopClient_clicked();
  void              on_m_btn_SendClient_clicked();
  void              on_m_btn_ReadClient_clicked();
  void              on_m_btn_ServerClear_clicked();
  void              on_m_btn_ClientClear_clicked();
  void              on_m_edit_StrngServer_textChanged();
  void              on_m_check_ServRnd_clicked(bool checked);
  void              slotOpenAboutForm();

  // Tray menu handlers
  void              changeEvent(QEvent*);
  void              trayIconActivated(QSystemTrayIcon::ActivationReason reason);
  void              trayActionExecute();
  void              setTrayIconActions();
  void              showTrayIcon();
  void              setTrayIcon(const QString ico_path);
  void              on_m_btn_update_comports_clicked();
  void              on_m_btn_cominfo_clicked();
  void              on_m_btn_comcnctdiscnct_clicked();
  void              on_m_btn_comlogclear_clicked();
  void              on_m_btn_comsend_clicked();
};

#endif // MAINWINDOW_H
