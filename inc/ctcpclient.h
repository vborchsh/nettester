#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QWidget>
#include <QTcpSocket>
#include <QThread>

#define TCP_TIMEOUT 1000

class ctcpclient : public QTcpSocket
{
  Q_OBJECT
public:
  explicit      ctcpclient(QTcpSocket *parent = nullptr);
  void          StartClient(const QString host, quint16 port);
  void          StopClient();

signals:
  void          signStrToLog(QString ostr);

public slots:
  void          slotConnected();
  void          slotDisconnected();
  void          bytesWritten(qint64 bytes);
  void          readyRead();
  void          slotSendToServ(QByteArray dat);
  void          slotReadServ();

private:
  QByteArray    m_RecieveArray;
};

#endif // TCPCLIENT_H
