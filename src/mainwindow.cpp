
#include "./inc/mainwindow.h"
#include "ui_mainwindow.h"

//------------------------------------------------------------------------------------------------
//
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  this->setTrayIconActions();
  this->showTrayIcon();

  setWindowIcon(QIcon(":/icons/ico_dis.ico"));
  setWindowTitle("TCP/IP Net tester v" + QString().sprintf("%d", VER)
                                 + "." + QString().sprintf("%d", SUB)
                                 + "." + QString().sprintf("%d", REV));

  m_Server = new ctcpserver();
  m_Server->moveToThread(&m_ServerThread);
  connect(m_Server, SIGNAL(signStrToLog(QString)       ), this    , SLOT(slotToServerLog(QString)) );
  connect(this    , SIGNAL(signSendToClient(QByteArray)), m_Server, SLOT(slotSendToClient(QByteArray)) );
  connect(this    , SIGNAL(signReadClient()            ), m_Server, SLOT(slotReadClient()) );
  m_ServerThread.start();

  m_Client = new ctcpclient();
  m_Client->moveToThread(&m_ClientThread);
  connect(m_Client, SIGNAL(signStrToLog  (QString)   ), this    , SLOT(slotToClientLog(QString))  );
  connect(this    , SIGNAL(signSentToServ(QByteArray)), m_Client, SLOT(slotSendToServ(QByteArray)));
  connect(this    , SIGNAL(signReadServ  ()          ), m_Client, SLOT(slotReadServ  ())          );
  m_ClientThread.start();

  SetGuiServ(false);
  SetGuiClient(false);

  ui->m_combo_comdbits->addItem("8", QSerialPort::Data8);
  ui->m_combo_comdbits->addItem("7", QSerialPort::Data7);
  ui->m_combo_comdbits->addItem("6", QSerialPort::Data6);
  ui->m_combo_comdbits->addItem("5", QSerialPort::Data5);

  ui->m_combo_comparity->addItem("None", QSerialPort::NoParity);
  ui->m_combo_comparity->addItem("Even", QSerialPort::EvenParity);
  ui->m_combo_comparity->addItem("Odd", QSerialPort::OddParity);
  ui->m_combo_comparity->addItem("Space", QSerialPort::SpaceParity);
  ui->m_combo_comparity->addItem("Mark", QSerialPort::MarkParity);

  ui->m_combo_comsbits->addItem("1", QSerialPort::OneStop);
  ui->m_combo_comsbits->addItem("1.5", QSerialPort::OneAndHalfStop);
  ui->m_combo_comsbits->addItem("2", QSerialPort::TwoStop);

  m_Comport = new CComPort();
  connect(m_Comport, SIGNAL(signStrToLog(QString)), this, SLOT(slotToComLog(QString)));

  // Load settings
  QSettings settings ("conf.ini", QSettings::IniFormat);
  settings.beginGroup("Server");
  ui->m_edit_PortServer->setText(settings.value("server_port", "2005").toString());
  ui->m_edit_StrngServer->setText(settings.value("server_repsonse", "1234").toString());
  ui->m_check_AutoResponse->setChecked(settings.value("server_autoresp", true).toBool());
  settings.endGroup();
  settings.beginGroup("Client");
  ui->m_edit_IPClient->setText(settings.value("client_ip", "127.0.0.1").toString());
  ui->m_edit_PortClient->setText(settings.value("client_port", "2005").toString());
  ui->m_edit_StrngClient->setText(settings.value("client_repsonse", "1234").toString());
  settings.endGroup();
}


MainWindow::~MainWindow()
{
  m_ServerThread.quit();
  m_ServerThread.wait();

  m_ClientThread.quit();
  m_ClientThread.wait();

  // Save settings
  QSettings settings ("conf.ini", QSettings::IniFormat);
  settings.remove("");
  settings.beginGroup("Server");
  settings.setValue("server_port", ui->m_edit_PortServer->text());
  settings.setValue("server_repsonse", ui->m_edit_StrngServer->text());
  settings.setValue("server_autoresp", ui->m_check_AutoResponse->isChecked());
  settings.endGroup();
  settings.beginGroup("Client");
  settings.setValue("client_ip", ui->m_edit_IPClient->text());
  settings.setValue("client_port", ui->m_edit_PortClient->text());
  settings.setValue("client_repsonse", ui->m_edit_StrngClient->text());
  settings.endGroup();
  settings.sync();

  delete ui;
}


//------------------------------------------------------------------------------------------------
// Run server
void MainWindow::on_m_btn_StartServ_clicked()
{
  qDebug()<<"Start server...";
  m_Server->m_autoresp = ui->m_check_AutoResponse->isChecked();
  m_Server->m_strResp  = ui->m_edit_StrngServer->text();
  m_Server->m_nPort    = static_cast<quint16>(ui->m_edit_PortServer->text().toInt());
  m_Server->m_bRandom  = ui->m_check_ServRnd->isChecked();

  m_Server->StartServer();
  m_bServStatus = true;
  setWindowIcon(QIcon(":/icons/ico_ena.ico"));
  setTrayIcon(":/icons/ico_ena.ico");
  SetGuiServ(true);
}


//------------------------------------------------------------------------------------------------
// Stop server
void MainWindow::on_m_btn_StopServ_clicked()
{
  m_Server->StopServer();
  m_bServStatus = false;
  setWindowIcon(QIcon(":/icons/ico_dis.ico"));
  setTrayIcon(":/icons/ico_dis.ico");
  SetGuiServ(false);
}


//------------------------------------------------------------------------------------------------
// Set status of server GUI
void MainWindow::SetGuiServ(bool st)
{
  ui->m_btn_StartServ->setEnabled(!st);
  ui->m_btn_StopServ->setEnabled(st);
  ui->m_edit_PortServer->setEnabled(!st);
  ui->m_btn_SendServer->setEnabled(st);
  ui->m_check_AutoResponse->setEnabled(!st);
}


void MainWindow::slotToServerLog(QString istr)
{
  ui->m_LogServer->append(getTimeNow() + istr);
}


void MainWindow::slotToClientLog(QString istr)
{
  ui->m_LogClient->append(getTimeNow() + istr);
}


void MainWindow::on_m_btn_SendServer_clicked()
{
  QByteArray dat;
  dat.append(ui->m_edit_StrngServer->text());

  emit this->signSendToClient(dat);
}


void MainWindow::on_m_btn_ServerClear_clicked()
{
    ui->m_LogServer->clear();
}


void MainWindow::on_m_edit_StrngServer_textChanged()
{
  this->m_Server->m_strResp = ui->m_edit_StrngServer->text();
}


void MainWindow::on_m_check_ServRnd_clicked(bool checked)
{
  if (nullptr != m_Server) {
    this->m_Server->m_bRandom = checked;
  }
}


//------------------------------------------------------------------------------------------------
// Set status of client GUI
void MainWindow::SetGuiClient(bool st)
{
  ui->m_btn_StartClient->setEnabled(!st);
  ui->m_btn_StopClient->setEnabled(st);
  ui->m_edit_IPClient->setEnabled(!st);
  ui->m_edit_PortClient->setEnabled(!st);
  ui->m_btn_SendClient->setEnabled(st);
  ui->m_btn_ReadClient->setEnabled(st);
}


void MainWindow::on_m_btn_StartClient_clicked()
{
  SetGuiClient(true);
  qDebug()<<"Start client...";

  // TODO: Check valid port & IP data
  m_port = ui->m_edit_PortClient->text().toUShort();
  m_addr = ui->m_edit_IPClient->text();

  m_bClientStatus = true;
  m_Client->StartClient(m_addr, m_port);
}


void MainWindow::on_m_btn_StopClient_clicked()
{
  SetGuiClient(false);
  m_Client->StopClient();
  m_bClientStatus = false;

  qDebug()<<"Client down";
}


void MainWindow::on_m_btn_SendClient_clicked()
{
  QByteArray dat;
  dat.append(ui->m_edit_StrngClient->text());

  emit this->signSentToServ(dat);
}


void MainWindow::on_m_btn_ReadClient_clicked()
{
  QByteArray dat;
  emit this->signReadServ();
}


void MainWindow::on_m_btn_ClientClear_clicked()
{
  ui->m_LogClient->clear();
}


//------------------------------------------------------------------------------------------------
// About form
void MainWindow::slotOpenAboutForm()
{
  QMessageBox::information(this, "TCP/IP Net tester v" + QString().sprintf("%d", VER) + "." + QString().sprintf("%d", SUB),
                                 "Simple TCP/IP net tester\n\n"
                                 "MICRAN RPD, TOMSK, 2017-2019"
                          );
}


//------------------------------------------------------------------------------------------------
// COM port
void MainWindow::on_m_btn_update_comports_clicked()
{
  ui->m_combo_comports->clear();
  QList<QSerialPortInfo> list = m_Comport->getAvailablesPorts();

  for (int i = 0; i < list.size(); i++) {
    ui->m_combo_comports->addItem(list.at(i).portName());
  }
}


void MainWindow::on_m_btn_cominfo_clicked()
{
  QList<QString> list = m_Comport->getInfo(ui->m_combo_comports->currentIndex());

  for (int i = 0; i < list.size(); i++) {
    ui->m_text_com->append(list.at(i));
  }
}


void MainWindow::slotToComLog(QString istr)
{
  ui->m_text_com->append(getTimeNow() + istr);
}


//------------------------------------------------------------------------------------------------
// Connection to selected serial port
void MainWindow::on_m_btn_comcnctdiscnct_clicked()
{
  if (ui->m_btn_comcnctdiscnct->text() == "Connect") {
    t_comparam params;
    params.indx  = ui->m_combo_comports->currentIndex();
    params.baud  = static_cast<quint32>(ui->m_edit_comrate->text().toUInt());
    params.dbits = static_cast<QSerialPort::DataBits>(ui->m_combo_comdbits->currentData().type());
    params.sbits = static_cast<QSerialPort::StopBits>(ui->m_combo_comsbits->currentData().type());
    params.parit = static_cast<QSerialPort::Parity>(ui->m_combo_comparity->currentData().type());

    m_Comport->slotConnect(params);

    ui->m_btn_comcnctdiscnct->setText("Disconnect");
  }
  else {
    m_Comport->slotDisconnect();

    ui->m_btn_comcnctdiscnct->setText("Connect");
  }
}


void MainWindow::on_m_btn_comlogclear_clicked()
{
  ui->m_text_com->clear();
}


void MainWindow::on_m_btn_comsend_clicked()
{
  emit m_Comport->slotSent(ui->m_edit_comsend->text().toUtf8());
}


//------------------------------------------------------------------------------------------------
// Tray
void MainWindow::showTrayIcon()
{
  trayIcon = new QSystemTrayIcon(this);
  setTrayIcon(":/icons/ico_dis.ico");

  trayIcon->setContextMenu(trayIconMenu);

  connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)       ),
          this    , SLOT  (trayIconActivated(QSystemTrayIcon::ActivationReason))
          );

  this->trayIcon->setToolTip("Server-client application");
}


void MainWindow::setTrayIcon(const QString ico_path)
{
  QIcon trayImage(ico_path);
  trayIcon->setIcon(trayImage);
  trayIcon->show();
}


void MainWindow::trayActionExecute()
{
  QString serv;
  QString client;

  serv   = (m_bServStatus  ) ? ("Server: Run") : ("Server: Stopped") ;
  client = (m_bClientStatus) ? ("Client: Run") : ("Client: Stopped") ;

  trayIcon->showMessage("Status",
                        serv + "\r\n" + client,
                        QSystemTrayIcon::Information,
                        1000
                       );
}


void MainWindow::trayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
  switch (reason)
  {
    case QSystemTrayIcon::DoubleClick:
      if (this->isVisible()) {
        this->hide();
      }
      else {
        this->showNormal();
        this->activateWindow();
      }
      break;

    case QSystemTrayIcon::Context:
      startServerAction->setEnabled(!this->m_bServStatus);
      stopServerAction->setEnabled(this->m_bServStatus);
      minimizeAction->setVisible(this->isVisible());
      restoreAction->setVisible(!this->isVisible());
      break;

    case QSystemTrayIcon::MiddleClick:
      this->trayActionExecute();
      break;

    default:
      break;
  }
}


void MainWindow::setTrayIconActions()
{
  // Setting actions...
  startServerAction = new QAction("Start server", this);
  stopServerAction  = new QAction("Stop server", this);
  minimizeAction    = new QAction("Hide", this);
  restoreAction     = new QAction("Show", this);
  quitAction        = new QAction("Quit", this);

  connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));
  connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));
  connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
  connect(startServerAction, SIGNAL(triggered()), this, SLOT(on_m_btn_StartServ_clicked()));
  connect(stopServerAction, SIGNAL(triggered()), this, SLOT(on_m_btn_StopServ_clicked()));

  // Setting system tray's icon menu...
  trayIconMenu = new QMenu(this);
  trayIconMenu->addSeparator();
  trayIconMenu->addAction (startServerAction);
  trayIconMenu->addAction (stopServerAction);
  trayIconMenu->addSeparator();
  trayIconMenu->addAction (minimizeAction);
  trayIconMenu->addAction (restoreAction);
  trayIconMenu->addAction (quitAction);
}


void MainWindow::changeEvent(QEvent *event)
{
  QMainWindow::changeEvent(event);

  if (event->type() == QEvent::WindowStateChange) {
    if (isMinimized()) {
        this -> hide();
    }
  }
}
