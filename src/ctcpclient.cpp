
#include "./inc/ctcpclient.h"

// ------------------------------------------------------------------------------------------------
// Constructor
ctcpclient::ctcpclient(QTcpSocket *parent) : QTcpSocket(parent)
{
  //socket = new QTcpSocket(nullptr);
  connect(this, SIGNAL(connected())         , this, SLOT(slotConnected())     );
  connect(this, SIGNAL(disconnected())      , this, SLOT(slotDisconnected())  );
  connect(this, SIGNAL(readyRead())         , this, SLOT(readyRead())         );
  connect(this, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64)));
}


// ------------------------------------------------------------------------------------------------
// Connection to IP:PORT server
void ctcpclient::StartClient(const QString host, quint16 port)
{
  emit this->signStrToLog("Connecting to " + host + ":" + QString().sprintf("%d", port));

  this->connectToHost(host, port);

  if (false == this->waitForConnected(TCP_TIMEOUT)) {
    emit this->signStrToLog("Error: " + this->errorString());
  }
}


// ------------------------------------------------------------------------------------------------
// Stop work
void ctcpclient::StopClient()
{
  if (true == this->isOpen()) {
    this->close();
  }
}


// ------------------------------------------------------------------------------------------------
// Send string to server
void ctcpclient::slotSendToServ(QByteArray dat)
{
  if (true == this->isOpen()) {
    qint64 num = this->write(dat);

    if (0 < num) {
      if (false == this->waitForBytesWritten(TCP_TIMEOUT)) {
        emit this->signStrToLog("Server writing timeout");
      }
    }
    else if (-1 == num) {
      emit this->signStrToLog("Error access to server");
    }
  }
}


// ------------------------------------------------------------------------------------------------
// Read data from server
void ctcpclient::slotReadServ()
{
  QByteArray dat;

  if (true == this->isOpen()) {
    if (this->bytesAvailable() > 0) {
      dat = this->readAll();
      emit this->signStrToLog(QString().append(dat));
    }
    else {
      emit this->signStrToLog("Nothing to read");
    }
  }
}


// ------------------------------------------------------------------------------------------------
void ctcpclient::slotConnected()
{
  emit this->signStrToLog("Connected!");
}


// ------------------------------------------------------------------------------------------------
void ctcpclient::slotDisconnected()
{
  emit this->signStrToLog("Disconnected!");
}


// ------------------------------------------------------------------------------------------------
// Handler of count of writed bytes
void ctcpclient::bytesWritten(qint64 bytes)
{
  emit this->signStrToLog(QString().sprintf("Wroted [%lld] bytes", bytes));
}


// ------------------------------------------------------------------------------------------------
// Standart reading handler
void ctcpclient::readyRead()
{
  while (this->bytesAvailable() > 0) {
    m_RecieveArray = this->readAll();
    emit this->signStrToLog("Readed [ " + m_RecieveArray + " ]");
  }
}

