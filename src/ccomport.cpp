
#include "./inc/ccomport.h"

CComPort::CComPort(QObject *parent) : QObject(parent)
{
  comport = new QSerialPort();
  connect(comport, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));

  cominfo = new QSerialPortInfo();
}


//------------------------------------------------------------------------------------------------
// Get availabled ports
QList<QSerialPortInfo> CComPort::getAvailablesPorts(void)
{
  return cominfo->availablePorts();
}


//------------------------------------------------------------------------------------------------
// Get info about selected comport
QList<QString> CComPort::getInfo(int num)
{
  QSerialPortInfo port = cominfo->availablePorts().at(num);
  QList<QString> list;
  list.append(QString("Port invalid... return"));

  if (true == port.isValid()) {
    list.clear();
    list.append(port.portName());
    list.append(port.manufacturer());
    list.append(port.serialNumber());
    list.append(port.description());
  }

  return list;
}


void CComPort::slotConnect(t_comparam p)
{
  comport->setPort(cominfo->availablePorts().at(p.indx));

  qDebug() << "Valid?" << cominfo->isValid();
  qDebug() << "Busy?" <<  cominfo->isBusy();

  if (true == comport->open(QSerialPort::ReadWrite)) {
    qDebug() << "Baudrate" << comport->setBaudRate(p.baud);
    qDebug() << "Databits" << comport->setDataBits(p.dbits);
    qDebug() << "Parity" << comport->setParity(p.parit);
    qDebug() << "Stopbits" << comport->setStopBits(p.sbits);
    qDebug() << "Flowcontrol" << comport->setFlowControl(QSerialPort::NoFlowControl);

    qDebug() << comport->write("\r\n");

    if (true == comport->waitForBytesWritten()) {
      signStrToLog(comport->portName() + " openned!");
    }
  }
  else {
    signStrToLog("Can't open " + comport->portName() + " error: " + QString().sprintf("%d", comport->error()));
  }
}


void CComPort::slotDisconnect(void)
{
  if (true == comport->isOpen()) {
    comport->close();
    signStrToLog(comport->portName() + " closed!");
  }
}


void CComPort::slotReadyRead(void)
{
  qDebug() << "Readed:" << comport->readAll();
}


void CComPort::slotSent(QByteArray dat)
{
  dat.append("\r\n");
  comport->write(dat);
  comport->waitForBytesWritten();
}
