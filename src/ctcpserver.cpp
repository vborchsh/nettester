
#include "./inc/ctcpserver.h"

// ------------------------------------------------------------------------------------------------
// Constructor
ctcpserver::ctcpserver(QTcpServer *parent) : QTcpServer(parent)
{
  connect(this, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));
}


// ------------------------------------------------------------------------------------------------
// Run listening on user port
void ctcpserver::StartServer()
{
  if (true == this->listen(QHostAddress::Any, m_nPort)) {
    emit this->signStrToLog("Server up");
  }
  else {
    emit this->signStrToLog("Server can't starting... return");
    emit this->StopServer();
  }
}


// ------------------------------------------------------------------------------------------------
// Stop work of server
void ctcpserver::StopServer()
{
  if (true == m_clientSock->isOpen()) {
    m_clientSock->close();
  }

  if (true == this->isListening()) {
    this->close();
  }

  emit this->signStrToLog("Server down");
}


// ------------------------------------------------------------------------------------------------
// Handler of new client conenction
void ctcpserver::slotNewConnection()
{
  m_clientSock = this->nextPendingConnection();

  if (true == m_autoresp) {
    QByteArray dat;
    dat.append(m_strResp);
    emit this->slotSendToClient(dat);
  }

  connect(m_clientSock, SIGNAL(readyRead())         , this, SLOT(slotReadClient())        );
  connect(m_clientSock, SIGNAL(disconnected())      , this, SLOT(slotClientDisconnected()));
  connect(m_clientSock, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64))    );

  emit this->signStrToLog("New client connected");
}


// ------------------------------------------------------------------------------------------------
//
void ctcpserver::slotReadClient()
{
  QByteArray dat;

  while (m_clientSock->bytesAvailable() > 0) {
    dat = m_clientSock->readAll();
    emit this->signStrToLog("Readed [ " + dat + " ]");
  }
}


// ------------------------------------------------------------------------------------------------
// Function to write user string to client
void ctcpserver::slotSendToClient(QByteArray dat)
{
  if (true == m_clientSock->isValid()) {
    qint64 num = m_clientSock->write(dat);

    if (num > 0) {
      m_clientSock->flush();
      m_clientSock->waitForBytesWritten(TCP_TIMEOUT);
    }
  }
}


// ------------------------------------------------------------------------------------------------
// Handler of count of writed bytes
void ctcpserver::bytesWritten(qint64 bytes)
{
  emit this->signStrToLog(QString().sprintf("Wroted [%lld] bytes", bytes));
}


// ------------------------------------------------------------------------------------------------
// After client disconnected
void ctcpserver::slotClientDisconnected()
{
  emit this->signStrToLog("Client is disconnected");
  m_clientSock->close();
}
